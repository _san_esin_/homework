from itertools import islice#just leave a comment 
import math
weights = []
height = []
with open("hw.txt") as f:

    for line in islice(f, 1, None):
        line = line.rstrip().split(',')
        height.append(float(line[1]))
        weights.append(float(line[2]))

avg_heights = sum(height) / len(height)
avg_weights = sum(weights) / len(weights)


x_weight_list = []

for el in weights:
    diff = (el - avg_weights)**2
    x_weight_list.append(diff)

D_weight = sum(x_weight_list)/len(x_weight_list)
weight_otclon = math.sqrt(D_weight)

x_height_list = []

for el in height:
    diff = (el - avg_heights) ** 2
    x_height_list.append(diff)

D_height = sum(x_height_list)/len(x_height_list)
height_otclon = math.sqrt(D_height)

print(avg_heights)
print(avg_weights)
print(height_otclon)
print(weight_otclon)
